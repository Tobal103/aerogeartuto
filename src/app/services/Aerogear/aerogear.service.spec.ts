import { TestBed } from '@angular/core/testing';

import { AerogearService } from './aerogear.service';

describe('AerogearService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AerogearService = TestBed.get(AerogearService);
    expect(service).toBeTruthy();
  });
});
