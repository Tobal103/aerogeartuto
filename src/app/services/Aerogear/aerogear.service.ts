import { Injectable } from '@angular/core';
import {config} from "./mobile-services";
@Injectable({
  providedIn: 'root'
})
export class AerogearService {

  constructor() { }


  public registerDevice(){
    /**Esta función es la que se encarga de registrar el dispositivo a aerogear, necesita que le pases tres handlers
     * @onNotification => Cuando reciva una notificación
     * @succesHandler => Cuando se registre correctamente
     * @errorHandler => Cuando falle el registro
     * 
     * a parte necesita la configuración
     * @config => configuración del proyecto
     * 
     * Hay que tener en cuenta que esta función, al formar parte de una libreria de cordova, solo funcionará cuando sea ejecutada desde un dispositivo,
     * con el ionic serve fallará diciendo que no se ha encontrado el plugin.
     * A parte, tiene que ser ejecutada una vez la plataforma este lista, es decir, despues de la ejecución de this.platform.ready, en app.component.ts
    */
    (<any>window).push.register(this.onNotification,this.succesHandler,this.errorHandler,config);
  }

  /**Es método se ejecutará cada vez que recibas una notificatión 
   * @param1 es la notificación recibida por aerogear
  */
  public onNotification(param1?){
    console.log(param1)
    console.log("notification recieved")
  }

  /**Este método se ejecuta cuando se registra a aerogear correctamente, no recive ningún parámetro*/
  public succesHandler(){
    console.log("success")
  }

  /**Este método se ejecuta cuando hay un error al intentar registrarse a Aerogear
   * @el recibe el error ocurrido al registrarse, es una cadena de texto.
  */
  public errorHandler(param1?){
    console.log(param1)
    console.log("error")
  }
}
