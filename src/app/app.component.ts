import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AerogearService } from './services/Aerogear/aerogear.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private aerogear:AerogearService// <= LLamamos el servicio donde queramos hacer el registro, (en mi caso nada mas iniciar la app)
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if(this.platform.is('cordova')){
        this.aerogear.registerDevice();//<=Ejecutamos el método para registrarnos, cuando podemos asegurar que la plataforma esta lista, y que dicha plataforma es cordova
      }
    });
  }
}
